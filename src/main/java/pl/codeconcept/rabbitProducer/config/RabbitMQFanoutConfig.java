package pl.codeconcept.rabbitProducer.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQFanoutConfig {

    @Bean
    Queue teksas1Queue() {
        return new Queue("teksas1Queue", false);
    }

    @Bean
    Queue teksas2Queue() {
        return new Queue("teksas2Queue", false);
    }

    @Bean
    Queue teksas3Queue() {
        return new Queue("teksas3Queue", false);
    }

    @Bean
    FanoutExchange exchange() {
        return new FanoutExchange("fanout-exchange");
    }

    @Bean
    Binding teksas1Binding(Queue teksas1Queue, FanoutExchange exchange) {
        return BindingBuilder.bind(teksas1Queue).to(exchange);
    }

    @Bean
    Binding teksas2Binding(Queue teksas2Queue, FanoutExchange exchange) {
        return BindingBuilder.bind(teksas2Queue).to(exchange);
    }

    @Bean
    Binding teksas3Binding(Queue teksas3Queue, FanoutExchange exchange) {
        return BindingBuilder.bind(teksas3Queue).to(exchange);
    }

}
